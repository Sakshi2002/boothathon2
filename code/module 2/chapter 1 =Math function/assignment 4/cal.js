function add() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = n1 + n2;
    document.getElementById("p1").innerHTML = "Addition of 2 no's=" + n1 + "+" + n2 + "<p><span style=color:red>Addition of 2 no's is <u><i>" + a.toString() + "</u></i></p></span>";
}
function sub() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = n1 - n2;
    document.getElementById("p1").innerHTML = "Substraction of 2 no's=" + n1 + "-" + n2 + "<p><span style=color:red>Substraction of 2 no's is <u><i>" + a.toString() + "</u></i></p></span>";
}
function div() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = n1 / n2;
    document.getElementById("p1").innerHTML = "Division of 2 no's=" + n1 + "/" + n2 + "<p><span style=color:red>Division of 2 no's is <u><i>" + a.toString() + "</u></i></p></span>";
}
function mult() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = n1 * n2;
    document.getElementById("p1").innerHTML = "Multiplication of 2 no's=" + n1 + "*" + n2 + "<p><span style=color:red>Multiplication of 2 no's is <u><i>" + a.toString() + "</u></i></p></span>";
}
function sin() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = Math.sin(n1 * Math.PI / 180);
    var b = Math.sin(n2 * Math.PI / 180);
    document.getElementById("p1").innerHTML = "Sine of A=sin(" + n1 + ")<br/><span style=color:red>Sine of A is <u><i>" + a.toString() + "</i></u></span><p>Sine of B=sin(" + n2 + ")<br/><span style=color:red>Sine of B is <u><i>" + b.toString() + "</i></u></p></span>";
}
function cos() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = Math.cos(n1 * Math.PI / 180);
    var b = Math.cos(n2 * Math.PI / 180);
    document.getElementById("p1").innerHTML = "Cosine of A=cos(" + n1 + ")<br/><span style=color:red>Cosine of A is <u><i>" + a.toString() + "</i></u></span><p>Cosine of B=cos(" + n2 + ")<br/><span style=color:red>Cosine of B is <u><i>" + b.toString() + "</i></u></p></span>";
}
function tan() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = Math.tan(n1 * Math.PI / 180);
    var b = Math.tan(n2 * Math.PI / 180);
    document.getElementById("p1").innerHTML = "Tan of A=tan(" + n1 + ")<br/><span style=color:red>Tan of A is <u><i>" + a.toString() + "</i></u></span><p>Tan of B=tan(" + n2 + ")<br/><span style=color:red>Tan of B is <u><i>" + b.toString() + "</i></u></p></span>";
}
function sr() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = Math.sqrt(n1);
    var b = Math.sqrt(n2);
    document.getElementById("p1").innerHTML = "Square Root of A=(" + n1 + ")<sup>1/2</sup><br/><span style=color:red>Square Root of A is <u><i>" + a.toString() + "</i></u></span><p>Square Root of B=(" + n2 + ")<sup>1/2</sup><br/><span style=color:red>Square Root of B is <u><i>" + b.toString() + "</i></u></p></span>";
}
function power() {
    let no1 = document.getElementById("t2");
    let no2 = document.getElementById("t3");
    var n1 = parseFloat(no1.value);
    var n2 = parseFloat(no2.value);
    var a = Math.pow(n1, n2);
    document.getElementById("p1").innerHTML = "Power = " + n1 + "<sup>" + n2 + "</sup><p style=color:red>Power is <u><i>" + a.toString() + "</u></i></p>";
}
//# sourceMappingURL=cal.js.map