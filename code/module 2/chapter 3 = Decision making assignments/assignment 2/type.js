function tot() {
    let X1 = document.getElementById("t1");
    let Y1 = document.getElementById("t2");
    let X2 = document.getElementById("t3");
    let Y2 = document.getElementById("t4");
    let X3 = document.getElementById("t5");
    let Y3 = document.getElementById("t6");
    var x1 = parseFloat(X1.value);
    var y1 = parseFloat(Y1.value);
    var x2 = parseFloat(X2.value);
    var y2 = parseFloat(Y2.value);
    var x3 = parseFloat(X3.value);
    var y3 = parseFloat(Y3.value);
    var a = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));
    var b = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2));
    var c = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    var s = a + b + c;
    var l;
    var r;
    if (a > b) {
        if (a > c) {
            l = a;
        }
        else {
            l = c;
        }
    }
    else {
        l = b;
    }
    r = s - l;
    if (r > l) {
        document.getElementById("p1").innerHTML = "Lenght of a = <i><u>" + a.toString() + "cm</u></i><br/>Lenght of b = <i><u>" + b.toString() + "cm</u></i><br/>Lenght of c = <i><u>" + c.toString() + "cm</u></i><br/>This sides forms a triangle. ";
        if (a == b && b == c) {
            if (a == (Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2))) || b == (Math.sqrt(Math.pow(a, 2) + Math.pow(c, 2))) || c == (Math.sqrt(Math.pow(b, 2) + Math.pow(a, 2)))) {
                document.getElementById("p2").innerHTML = "<i>The Triangle is a Right Angled Equilateral triangle.</i>";
            }
            else {
                document.getElementById("p2").innerHTML = "<i>The Triangle is a Equilateral triangle.</i>";
            }
        }
        else if ((a != b && b == c) || (a == b && b != c) || (a == c && c != b)) {
            if (a == (Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2))) || b == (Math.sqrt(Math.pow(a, 2) + Math.pow(c, 2))) || c == (Math.sqrt(Math.pow(b, 2) + Math.pow(a, 2)))) {
                document.getElementById("p2").innerHTML = "<i>The Triangle is a Right Angled Isosceles triangle.</i>";
            }
            else {
                document.getElementById("p2").innerHTML = "<i>The Triangle ABC is a Isosceles triangle.</i>";
            }
        }
        else if (a != b && b != c) {
            if (a == (Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2))) || b == (Math.sqrt(Math.pow(a, 2) + Math.pow(c, 2))) || c == (Math.sqrt(Math.pow(b, 2) + Math.pow(a, 2)))) {
                document.getElementById("p2").innerHTML = "<i>The Triangle ABC is a Right Angled Isosceles triangle.</i>";
            }
            else {
                document.getElementById("p2").innerHTML = "<i>The Triangle ABC is a scalene triangle.</i>";
            }
        }
    }
    else {
        document.getElementById("p1").innerHTML = "<i style=color:red>This points does not forms a triangle.</i> ";
    }
}
//# sourceMappingURL=type.js.map